<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="dev_tileset" tilewidth="100" tileheight="100" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="100" height="100" source="../images/tile_1.png"/>
 </tile>
 <tile id="1">
  <image width="100" height="100" source="../images/tile_2.png"/>
 </tile>
 <tile id="2">
  <image width="100" height="100" source="../images/tile_3.png"/>
 </tile>
 <tile id="3">
  <image width="100" height="100" source="../images/tile_down.png"/>
 </tile>
 <tile id="4">
  <image width="100" height="100" source="../images/tile_left.png"/>
 </tile>
 <tile id="5">
  <image width="100" height="100" source="../images/tile_right.png"/>
 </tile>
 <tile id="6">
  <image width="100" height="100" source="../images/tile_up.png"/>
 </tile>
 <tile id="7">
  <image width="100" height="100" source="../images/tile_start.png"/>
 </tile>
</tileset>
