const FLOOR_PIECE_SCALE             = 1.0;
const FLOOR_FALL_ANIMATION_DURATION = 1000;


class FloorPiece
{
    //--------------------------------------------------------------------------
    constructor(x, y, w, h, strength, type)
    {
        this.pos      = pw_Vector_Create(x, y);
        this.size     = pw_Vector_Create(w, h);
        this.scale    = FLOOR_PIECE_SCALE;
        this.strength = strength;
        this.type     = type;

        this.is_player_start = false;

        this.fall_end_angle = pw_Random_Number(0, PW_MATH_PI);
        this.fall_tween     = null;
    } // CTOR

    //--------------------------------------------------------------------------
    DecrementStrength()
    {
        --this.strength;
        if(this.strength <= 0) {
            this.fall_tween = pw_Tween_CreateBasic(FLOOR_FALL_ANIMATION_DURATION)
                .onStart(()=>{
                })
                .onUpdate((v)=>{
                    this.scale = FLOOR_PIECE_SCALE - pw_Math_Sin(PW_MATH_PI_OVER_2 * v.value);
                })
                .onComplete(()=>{
                })
                .start();
        }
    } // DecrementStrength

    //--------------------------------------------------------------------------
    Draw()
    {
        pw_Canvas_Push();
            // Canvas_Translate(-this.pos.x * 0.5, -this.pos.y * 0.5);
            pw_Canvas_Translate(this.pos.x, this.pos.y);
            pw_Canvas_Scale(this.scale);
            if(this.strength <= 0) {
                pw_Canvas_Rotate(this.scale * this.fall_end_angle);
            }
            pw_Canvas_SetFillStyle("white");
            pw_Canvas_FillRect(
                -this.size.x * 0.5,
                -this.size.y * 0.5,
                this.size.x - 2,
                this.size.y - 2
            );
        pw_Canvas_Pop();
    } // Draw
}; // class FloorPiece


//----------------------------------------------------------------------------//
// Floor Factory                                                              //
//----------------------------------------------------------------------------//
function
FloorCreate(x, y, w, h, type)
{
    type = pw_Math_ToInt(type);

    // @incomplete: Add the enemies blocks...
    const _is_block_start = (type)=>{
        return (type == 7);
    }
    const _is_block_static = (type)=>{
        return (type >= 0 && type <= 2);
    };
    const _is_block_moving = (type)=>{
        return type >= 3 && type <= 6;
    };

    if(_is_block_start(type)) {
        const floor = new FloorPiece(x, y, w, h, Infinity, type);
        floor.is_player_start = true;
        return floor;
    }
    if(_is_block_static(type)) {
        return new FloorPiece(x, y, w, h, type + 1, type);
    }
    // @incomplete: Add subclass for the moving blocks...
    if(_is_block_moving(type)) {
        return new FloorPiece(x, y, w, h, Infinity, type);
    }
    debugger;
    return null;

}
