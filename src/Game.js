
class Game
{
    //--------------------------------------------------------------------------
    constructor()
    {
        this.floor  = null;
        this.player = null;
    } // CTOR

    //--------------------------------------------------------------------------
    async Reset(level)
    {
        this.floor  = new LevelFloor(level);
        await this.floor.LoadFloorData();

        this.player = new Player(
            this.floor.player_start_tile.pos.x,
            this.floor.player_start_tile.pos.y,
            this.floor.tile_size.x,
            this.floor.tile_size.y
        );

    } // Reset

    //--------------------------------------------------------------------------
    Update()
    {
        this.player.Update();
        this.floor .Update();

        if(!this.player.is_moving) {
            const desired_direction = this.player.GetDesiredDirection();
            if(desired_direction) {
                const current_tile = this.floor.PositionToTile(this.player.pos.x, this.player.pos.y);
                const target_tile  = pw_Vector_Create(
                    desired_direction.x + current_tile.x,
                    desired_direction.y + current_tile.y
                );

                const target_pos = this.floor.TileToPosition(target_tile.x, target_tile.y);
                this.player.MoveToPos(target_pos.x, target_pos.y);
            }
        }
        if(this.player.just_finished_moving) {
            this.player.just_finished_moving = false;
            const dst_tile = this.floor.GetTileAtPosition(
                this.player.pos.x,
                this.player.pos.y
            );
            const src_tile = this.floor.GetTileAtPosition(
                this.player.start_pos.x,
                this.player.start_pos.y
            );
            src_tile.DecrementStrength();

            // Moved to a empty floor.
            if(!dst_tile || dst_tile.strength <= 0) {
                this.player.Die();
            }
        }

    } // Update

    //--------------------------------------------------------------------------
    Draw()
    {
        this.floor .Draw();
        this.player.Draw();
    } // Draw

} // class Game
