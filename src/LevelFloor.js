
class LevelFloor
{
    constructor(level_number)
    {
        // HouseKeeping
        this.level_number = level_number;
        this.tile_size    = null;
        this.rows         = null;
        this.cols         = null;
        // Floor Data
        this.static_floor = null;
        this.moving_floor = null;
        this.enemies      = null;
        // Player Data
        this.player_start_tile = null;
    } // CTOR

    //--------------------------------------------------------------------------
    PositionToTile(x, y)
    {
        return pw_Vector_Create(
            pw_Math_Int(x / this.tile_size.x),
            pw_Math_Int(y / this.tile_size.y)
        );
    } // PositionToTile

    //--------------------------------------------------------------------------
    TileToPosition(x, y)
    {
        return pw_Vector_Create(
            pw_Math_Int(x * this.tile_size.x),
            pw_Math_Int(y * this.tile_size.y)
        );
    } // TileToPos

    //--------------------------------------------------------------------------
    IsValidTile(x, y)
    {
        return y >= 0 && y < this.rows
            && x >= 0 && x < this.cols;
    } // IsValidTile

    //--------------------------------------------------------------------------
    GetTileAtPosition(x, y)
    {
        const target_tile = this.PositionToTile(x, y);
        if(!this.IsValidTile(target_tile.x, target_tile.y)) {
            return null;
        }

        // Static Floor.
        if(this.static_floor[target_tile.y][target_tile.x]) {
            return this.static_floor[target_tile.y][target_tile.x];
        }

        // Moving Floor.
        const moving_tile = pw_Array_FindIf(this.moving_floor, (tile)=>{
            return pw_Rect_Intersects(
                         x,          y, this.tile_size.x, this.tile_size.y,
                tile.pos.x, tile.pos.y, this.tile_size.x, this.tile_size.y
            );
        });
        if(moving_tile) {
            return moving_tile;
        }

        // Enemy.
        const enemy = pw_Array_FindIf(this.enemies, (tile)=>{
            return pw_Rect_Intersects(
                         x,            y, this.tile_size.x, this.tile_size.y,
                tile.pos.x,   tile.pos.y, this.tile_size.x, this.tile_size.y
            );
        });
        if(!enemy) {
            return enemy;
        }

        return null;
    } // GetTileAtPosition

    //--------------------------------------------------------------------------
    async LoadFloorData()
    {
        const url      = pw_String_Cat("assets/maps/map", this.level_number + 1, ".csv");
        const csv_text = await pw_LoadDataAsync(url);

        // @notice: Today I'm writing the function as str.split(",") but I don't
        // have much idea if this is faster than parsing the actual data and
        // creating a tile when we hit the comma.
        // Right now my bet would that it might be faster in small lists due
        // the optimizations of the function on the javascript side, but it
        // will require a O(n) memory to hold the entire list.
        //
        // Actually would be nice to write a small benchmark of this later ;D
        // stdmatt - Jul 26, 2020
        let lines = csv_text.split("\n");
        if(pw_Array_GetBack(lines).length == 0) {
            pw_Array_PopBack(lines);
        }

        this.rows         = lines.length;
        this.cols         = lines[0].split(",").length;
        this.tile_size    = pw_Vector_Create(50, 50);
        this.static_floor = pw_Array_Create2D(this.rows, this.cols);

        for(let i = 0; i < this.rows; ++i) {
            const values = lines[i].split(",");
            for(let j = 0; j < this.cols; ++j) {
                const floor_type = values[j];
                const floor = FloorCreate(
                    j * this.tile_size.x,
                    i * this.tile_size.y,
                    this.tile_size.x,
                    this.tile_size.y,
                    floor_type
                );

                this.static_floor[i][j] = floor;
                if(floor.is_player_start) {
                    this.player_start_tile = floor;
                };
            }
        }
    } // LoadFloorData


    //--------------------------------------------------------------------------
    Update()
    {
    } // Update

    //--------------------------------------------------------------------------
    Draw()
    {
        for(let i = 0; i < this.rows; ++i) {
            for(let j = 0; j < this.cols; ++j) {
                const tile = this.static_floor[i][j];
                tile.Draw()
            }
        }

        if(this.moving_floor) {
            for(let i = 0; i < this.moving_floor.length; ++i) {
                const tile = this.moving_floor[i];
                tile.Draw();
            }
        }

        if(this.enemies) {
            for(let i = 0; i < this.enemies.length; ++i) {
                const tile = this.enemies[i];
                tile.Draw();
            }
        }
    } // Draw


} // class LevelFloor
